package com.example.mav.example0;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class NameListActivity extends ListActivity {
    public final static String EXTRA_MESSAGE = "com.example.mav.example0.MESSAGE";
    List<Human> humans;
    class Human {
        String name;
        String surname;

        Human(String names, String surnames) {
            name = names;
            surname = surnames;
        };

    }

    public List<Human> getNameList() {
        List<Human> humans = new ArrayList<Human>();
        humans.add(new Human( "Anton" , "Sharipov" ));
        humans.add(new Human( "Alexey" , "Vinogradov" ));
        humans.add(new Human( "Alan" , "Dzagoev" ));
        humans.add(new Human( "Nikolay" , "Petrovich" ));
        humans.add(new Human( "Dima" , "Petrov" ));
        humans.add(new Human( "Michail" , "Lenin" ));
        humans.add(new Human( "Anton" , "Shmakov" ));
        humans.add(new Human( "Masha" , "Shmakova" ));
        humans.add(new Human( "Tatiana" , "Volnova" ));
        humans.add(new Human( "Ksenia" , "Svalova" ));
        humans.add(new Human( "Brad" , "Pitt" ));
        humans.add(new Human( "David" , "Goliaphov" ));
        humans.add(new Human( "Spider" , "Man" ));
        humans.add(new Human( "Pr." , "Octopus" ));
        humans.add(new Human( "Andrey" , "Korobejnikov" ));

        return humans;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        humans = getNameList();

        List<String> list = new ArrayList<String>(15);
        for (int i=1;i<16;i++){
            list.add(i+": " + humans.get(i-1).name + " " + humans.get(i-1).surname);
        }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, list);
        setListAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.name__list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        if (position < humans.size()) {
            invokePassport(humans.get(position).name,humans.get(position).surname);
        }

    }

    public void invokePassport(String name, String surname) {
        Intent intent = new Intent(this, PassportActivity.class);
        String[] suname = new String[]{name,surname};
        intent.putExtra(EXTRA_MESSAGE,suname); //<-----------------разобраться с отправкой данных
        startActivity(intent);
    }
}
