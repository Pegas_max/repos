package com.example.mav.example0;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by mav on 20.10.2014.
 */
public class PassportActivity extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set the text view as the activity layout
        setContentView(R.layout.ac_passport);

        // Get the message from the intent
        Intent intent = getIntent();
        String[] message = intent.getStringArrayExtra(NameListActivity.EXTRA_MESSAGE);

        // Work with text view
        TextView name = (TextView) findViewById(R.id.name);
        TextView surname = (TextView) findViewById(R.id.surname);

        name.setText(message[0]);
        surname.setText(message[1]);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
